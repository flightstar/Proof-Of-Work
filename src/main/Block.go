package main

import (
	"strconv"
	"bytes"
	"crypto/sha256"
	"time"
	"fmt"
	"math"
	"math/big"
)

type Block struct {
	Index		  int
	Timestamp     int64
	Data          []byte
	PrevBlockHash []byte
	Hash          []byte
}
func (b *Block) SetHash() {
	timestamp := []byte(strconv.FormatInt(b.Timestamp, 10))
	headers := bytes.Join([][]byte{b.PrevBlockHash, b.Data, timestamp}, []byte{})
	hash := sha256.Sum256(headers)
	b.Hash = hash[:]
}

var index = 0
func NewBlock(data string,  prevBlockHash []byte) *Block {
	index += 1
	block := &Block{index,time.Now().Unix(),
		[]byte(data), prevBlockHash, []byte{}}
	block.SetHash()
	return block
}

func main()  {
	fmt.Printf("%f\n",math.MaxInt64)
	target := big.NewInt(1)
	a := target.Lsh(target, 256-24)
	fmt.Println(a)
}